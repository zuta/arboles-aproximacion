'''
Cálculo del número ópyimo de árboles.
'''

import sys

def compute_trees(trees):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit

    return production

def compute_all(min_trees, max_trees):
    productions = []
    for x in range (min_trees, max_trees + 1):
        productions.append(x, compute_trees(x))

    return productions

def read_arguments():
    if len(sys.argv) != 6:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> >min> <max>")

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])

    except ValueError:
        sys.exit("All arguments must be integers")

    return (base_trees, fruit_per_tree, reduction, min, max)


def main():
    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    best_production = 0
    best_trees = 0

    for x in range(0, len(productions)):
        print(productions[x][0], productions[x][1])
        if productions[x][1] > best_production:
            best_production = productions[x][1]
            best_trees = productions[x][0]

    print(f"Best production:{best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
